#!/usr/bin/env python3

from datetime import datetime
from pathlib import Path
import pdfplumber
import traceback
import openpyxl
import math
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
#################################################

bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

def fatura_oku():
	try:
		# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
		# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
		pdf_klasoru = Path('./PDF/').rglob('*.pdf')
		    # PDF Klasörü içindeki her bir dosyayı buluyoruz.
		files = [x for x in pdf_klasoru]
		    # PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
		sira_no = 0

		# Uçbirimde çıkacak sutun isimleri
		print("{:<3}{:<14}{:<55}{:<10}".format("S.N", "SÖZ./ABONE NO", "CAMİ/ABONE ADI", "TUTAR"))

		# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
		for file in files:
			birincisayfa = []			
			# pdfplumber modülü ile dosyaları açıyoruz
			with pdfplumber.open(file) as pdf:
				sayfa_1 = pdf.pages[0]
				birincisayfa.append(sayfa_1.extract_text())
			#print(birincisayfa)

			oncekiBorcTutari = float(0)
			sira_no = sira_no + 1
			# Sayfaları Geziyoruz
			for sayfa in birincisayfa:
				# Cami/Abone adını tespit etmek için kriter belirliyoruz.
				abone_eslesen = re.search("AADDII\s(\D+)\\nMM", sayfa)
				abone_adi = ' '.join(' '.join(abone_eslesen.groups()).strip().split("\n")[:2])

				# Toplam harcanan KWH
				kwh_eslesen = re.search(r"Bedeli.?([\d+]?[\d+]?[\d+]?\.?[\d+]?[\d+]?[\d+]?,?[\d+]?[\d+]?[\d+]?)", sayfa)
				kwh_toplam = float(' '.join(kwh_eslesen.groups()).replace('.', '').replace(',', '.').strip())

				# Fatura Tesisat/Tüketici Numarası
				tesisat_eslesen = re.search("SSAATT\D+(\d+)", sayfa)
				tesisat_no = ' '.join(tesisat_eslesen.groups()).strip()

				# Müsteri NO
				musteri_eslesen = re.search("NNOO.?(\d+)", sayfa)
				musteri_no = ' '.join(musteri_eslesen.groups())

				# Fatura Tesisat/Tüketici Numarası
				sozhesap_eslesen = re.search("AABBII\s?(\d+)", sayfa)
				sozhesap_no = ' '.join(sozhesap_eslesen.groups()).strip()

				# Fatura No
				fatura_eslesen = re.search("AG\s?(\w+)", sayfa)
				fatura_no = ' '.join(fatura_eslesen.groups()).strip()

				# Faturanın ait olduğu dönem
				faturaDonemi_eslesen = re.search("[F]\w{5}\D+\s(\d+\/\d+)", sayfa)
				faturaDonemi = ' '.join(faturaDonemi_eslesen.groups(1))

				# Son ödeme Tarihi
				sonOdeme_eslesen = re.search("z\\n(\w+.\w+.\w+).?\\n", sayfa)
				sonOdeme_tarihi = ' '.join(sonOdeme_eslesen.groups(1))

				# İlk Okuma Tarihi
				sonOkuma_eslesen = re.search("Son\sOkuma\s\w+\s(\w+.\w+.\w+)", sayfa)
				sonOkuma_tarihi = ' '.join(sonOkuma_eslesen.groups())

				# Son Okuma Tarihi
				ilkOkuma_eslesen = re.search(".lk\sOkuma\s\w+\s(\w+.\w+.\w+)", sayfa)
				ilkOkuma_tarihi = ' '.join(ilkOkuma_eslesen.groups())


				faturaTutari_eslesen = re.search("TL\\n([0-9]+.?[0-9]+.?\d+) TL\\nM", sayfa)
				faturaTutari = float(' '.join(faturaTutari_eslesen.groups()).replace(",", ".").replace(',', '.').strip())

				# fatura tutarına binde 948 ile çarpılarak bulunmuştur.
				damga_vergisi = round(((faturaTutari/1.18)*0.00948), 2)

				# Fatura tutarını 1.18 e bölüp yüzde 18 ile çarparak bulunmuştur.
				kdv = round(((faturaTutari/1.18)*0.18), 2)

				# Eğer varsa önceki döneme ait borç
				oncekiBorc_eslesen = re.search("\s*\\n([0-9.]*?)\s?\\nÖ?DEME", sayfa)
				oncekiBorcTutari = float(' '.join(oncekiBorc_eslesen.groups()))

				# Ödenecek Tutar
				odenecekTutar_eslesen = re.findall("\\n([0-9]+.?[0-9]+.?\d+) TL\\nE", sayfa)
				odenecekTutar = float(' '.join(odenecekTutar_eslesen).replace('.', '').replace(',', '.').strip())


			# Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
			print("{:<3}{:<14}{:<55}{:<10}".format(sira_no, sozhesap_no, abone_adi, oncekiBorcTutari))

			# ornek_excel.xlsx dosyasının son satırını buluyoruz.
			son_satir = excel_sayfasi.max_row

			# ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
			excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
			excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(musteri_no)
			excel_sayfasi.cell(column=3, row=son_satir + 1).value = int(sozhesap_no)
			excel_sayfasi.cell(column=4, row=son_satir + 1).value = int(tesisat_no)
			excel_sayfasi.cell(column=5, row=son_satir + 1).value = fatura_no
			excel_sayfasi.cell(column=6, row=son_satir + 1).value = abone_adi
			excel_sayfasi.cell(column=7, row=son_satir + 1).value = 0
			excel_sayfasi.cell(column=8, row=son_satir + 1).value = faturaDonemi
			excel_sayfasi.cell(column=9, row=son_satir + 1).value = ilkOkuma_tarihi
			excel_sayfasi.cell(column=10, row=son_satir + 1).value = sonOkuma_tarihi
			excel_sayfasi.cell(column=11, row=son_satir + 1).value = sonOdeme_tarihi
			excel_sayfasi.cell(column=12, row=son_satir + 1).value = kwh_toplam
			excel_sayfasi.cell(column=13, row=son_satir + 1).value = faturaTutari
			excel_sayfasi.cell(column=14, row=son_satir + 1).value = damga_vergisi
			excel_sayfasi.cell(column=15, row=son_satir + 1).value = kdv
			excel_sayfasi.cell(column=16, row=son_satir + 1).value = 0
			excel_sayfasi.cell(column=17, row=son_satir + 1).value = 0
			excel_sayfasi.cell(column=18, row=son_satir + 1).value = odenecekTutar

			# ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
			# pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
			excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')
	except:
		print("Dosya Adı: ", file)
		print(traceback.format_exc())

if __name__ == "__main__":
	fatura_oku()
