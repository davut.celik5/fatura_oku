#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from pathlib import Path
import pdfplumber
import traceback
import openpyxl
import math
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
#################################################

bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

def fatura_oku():
	try:

		# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
		# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
		pdf_klasoru = Path('./PDF/').rglob('*.pdf')

		# PDF Klasörü içindeki her bir dosyayı buluyoruz. 
		files = [x for x in pdf_klasoru]

		# PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
		sira_no = 0

		# Uçbirimde çıkacak sutun isimleri
		print("{:<5}{:<18}{:<45}{:<10}".format("S.N", "SÖZ./ABONE NO", "CAMİ/ABONE ADI", "ÖD.TUTAR"))
		print("{:<5}{:<18}{:<45}{:<10}".format("---", "-------------", "--------------", "--------"))

		# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
		for file in files:
			birincisayfa = []
			# pdfplumber modülü ile dosyaları açıyoruz
			with pdfplumber.open(file) as pdf:
				sayfa_1 = pdf.pages[0]
				birincisayfa.append(sayfa_1.extract_text())
			#print(birincisayfa)

			# Her bir pdf dosyası okununca sira numarası bir artacaktır.
			sira_no = sira_no + 1

			for sayfa in birincisayfa:
				# Abone Unvanı
				abone_eslesen = re.search("Soyad:(\D+)\\n", sayfa)
				abone_adi = ' '.join(abone_eslesen.groups()).split("\n")[0].strip()

				# Abone Unvanı --> Eğer sadece müftülük adı gelsin isteniyor ise aşağıdaki iki satırın önündeki # işareti kaldırın
				# ve yukarıdaki abone_eslesen ve abone_adi alanınlarının önüne # işareti koyun.
				#abone_eslesen = re.search("Adres:(\D+)[0-9]?(\D+)\\n", sayfa)
				#abone_adi = ' '.join(abone_eslesen.groups()).split("SOKAK")[0].strip()

				# Toplam harcanan KWH
				kwh_eslesen = re.search("ENER\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
				kwh_toplam = float(kwh_eslesen.group(1).strip().replace(".", "").replace(",", "."))

				# Sözleşme Hesap No
				sozhesap_eslesen = re.search("Kod\\n(\d+)\s(\w+)\s(\d+)\\n", sayfa)
				sozhesap_no = ' '.join(sozhesap_eslesen.groups()).split()[0].strip()

				# Tesisat No - Tekil Kod
				tesisat_eslesen = re.search("Kod\\n(\d+)\s(\w+)\s(\d+)\\n", sayfa)
				tesisat_no = ' '.join(tesisat_eslesen.groups()).split()[-1].strip()

				# Abonelik Grubu
				abonegrubu_eslesen = re.search("Grubu\s?:?(\D+)", sayfa)
				abonegrubu = ' '.join(abonegrubu_eslesen.groups()).split("Zamanlı")[1].split("AG")[0].strip()

				# Fatura No
				fatura_eslesen = re.search("Fatura\D+No[\s]?[:]?[\s]?[:]?(\w+)", sayfa)
				fatura_no = ' '.join(fatura_eslesen.groups()).strip()

				# Faturanın ait olduğu dönem
				faturaDonemi_eslesen = re.search("(\d+\/\d+)\s?U", sayfa)
				faturaDonemi = ' '.join(faturaDonemi_eslesen.groups()).strip().replace("-", "/")

				# Faturanın İlk ve Son Okuma Tarihleri
				ilksonOkuma_eslesen = re.search("Okuma\D+(\d+.\d+.\d+)\D+(\d+.\d+.\d+)", sayfa)
				ilkOkuma_tarihi = ' '.join(ilksonOkuma_eslesen.groups()).split()[0]
				sonOkuma_tarihi = ' '.join(ilksonOkuma_eslesen.groups()).split()[1]

				# Son Ödeme Tarihi
				sonOdeme_eslesen = re.search("SON\D+(.*)\\n\D+\\n\D+OKUMA", sayfa)
				sonOdeme_tarihi = ' '.join(sonOdeme_eslesen.groups()).split()[-2].strip()


				#Eski Borç Tutarı
				oncekiBorc_eslesen = re.search("Bor\D+Tut\D+[0-9]\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
				if oncekiBorc_eslesen == None:
					oncekiBorcTutari = float(0)
				else:
					oncekiBorcTutari = float(oncekiBorc_eslesen.group(1).strip().replace(".", "").replace(",", "."))

				# Fatura Tutarı
				faturaTutari_eslesen = re.search("Fatura.?Tutar\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
				faturaTutari = float(faturaTutari_eslesen.group(1).strip().replace(".", "").replace(",", "."))

				# Damga Vergisi
				damga_vergisi = round(((faturaTutari/1.18)*0.00948),2)
				
				# KDV
				kdv = round(((faturaTutari/1.18)*0.18),2)

				# Ödenecek Tutar
				odenecekTutar_eslesen = re.search("SON\D+(.*)\\n\D+\\n\D+OKUMA", sayfa)
				odenecekTutar = float(' '.join(sonOdeme_eslesen.groups()).split()[0].strip().replace(".", "").replace(",", "."))

			# Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
			print("{:<5}{:<18}{:<45}{:<10}".format(sira_no, sozhesap_no, abone_adi, odenecekTutar))

			# ornek_excel.xlsx dosyasının son satırını buluyoruz.
			son_satir = excel_sayfasi.max_row

			# ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
			excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
			excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(sozhesap_no)
			excel_sayfasi.cell(column=3, row=son_satir + 1).value = int(tesisat_no)
			excel_sayfasi.cell(column=4, row=son_satir + 1).value = fatura_no
			excel_sayfasi.cell(column=5, row=son_satir + 1).value = abone_adi
			excel_sayfasi.cell(column=6, row=son_satir + 1).value = abonegrubu
			excel_sayfasi.cell(column=7, row=son_satir + 1).value = datetime.strptime(faturaDonemi, "%Y/%m").strftime('%Y/%m')
			excel_sayfasi.cell(column=8, row=son_satir + 1).value = datetime.strptime(ilkOkuma_tarihi, "%d.%m.%Y").strftime('%Y/%m/%d')
			excel_sayfasi.cell(column=9, row=son_satir + 1).value = datetime.strptime(sonOkuma_tarihi, "%d.%m.%Y").strftime('%Y/%m/%d')
			excel_sayfasi.cell(column=10, row=son_satir + 1).value = datetime.strptime(sonOdeme_tarihi, "%d.%m.%Y").strftime('%Y/%m/%d')
			excel_sayfasi.cell(column=11, row=son_satir + 1).value = kwh_toplam
			excel_sayfasi.cell(column=12, row=son_satir + 1).value = faturaTutari
			excel_sayfasi.cell(column=13, row=son_satir + 1).value = damga_vergisi
			excel_sayfasi.cell(column=14, row=son_satir + 1).value = kdv
			excel_sayfasi.cell(column=15, row=son_satir + 1).value = odenecekTutar
			excel_sayfasi.cell(column=16, row=son_satir + 1).value = oncekiBorcTutari

			# ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
			# pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
			excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')

	except:
		print("Hata Veren PDF Dosyası :", file)
		print(traceback.format_exc())

if __name__ == "__main__":
	fatura_oku()
