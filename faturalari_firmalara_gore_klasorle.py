#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path
import natsort
import pathlib 
import shutil
import fitz
import re
import os


sirketler = ['AKEDAŞ', 'GEDİZ', 'AKSA FIRAT', 'DİCLE', 'YEPAŞ', 'ÇAMLIBEL', 'AKSA ÇORUH', 'TRAKYA', 'AYDEM', 'SAKARYA', 'OSMANGAZİ ELEKTRİK', 'ARAS', 'ENERJİSA BAŞKENT', 'ENERJİSA İSTANBUL', 'YEŞİLIRMAK', 'MERAM', 'KAYSERİ', 'LİMAK ULUDAĞ', 'GEDİZ', 'AKDENİZ']

klasor_yolu = str(pathlib.Path.cwd()) + "/PDF/"
dosya_yolu = ""
dosyanin_tam_yolu = ""
firma_klasoru = ""
dosyanin_yeni_adi = ""

def move_file(filepath, dest):
    # Convert to absolute paths.
    filepath = os.path.abspath(filepath)
    dest     = os.path.abspath(dest)
 
    # If destination is a directory, use full path, including filename.
    if os.path.isdir(dest):
        filename = os.path.basename(filepath)
        dest_filepath = os.path.join(dest, filename)
        shutil.move(filepath, dest_filepath)
    else:
        shutil.move(filepath, dest)
 
 
pdf_klasoru = Path('./PDF/').rglob('*.pdf')
files = [x for x in pdf_klasoru]

sirket_sira_no = []
for file in natsort.os_sorted(files):
    with fitz.open(file) as pdf:
        sayfa_1 = pdf[0]
        text = sayfa_1.get_text().strip().replace('\n', ' ').upper()
        
        for sirket in sirketler:
            if sirket in text:
                firma_adi = '{}'.format(sirket.upper())
                sirket_sira_no.append(firma_adi)

                dosya_yolu = str(pathlib.Path(file))
                dosyanin_tam_yolu = os.path.abspath(file)
                firma_klasoru = str(klasor_yolu + firma_adi)
                dosyanin_yeni_adi = klasor_yolu + firma_adi + "_" + str(sirket_sira_no.count(firma_adi)) + ".pdf"

        print(dosyanin_tam_yolu, " dosyası, ", dosyanin_yeni_adi, " olarak değiştirildi!")
        os.rename(dosyanin_tam_yolu, dosyanin_yeni_adi)

        if os.path.isdir(firma_klasoru) == False :
            os.mkdir(firma_klasoru)
            move_file(dosyanin_yeni_adi, firma_klasoru)
        else:
            #print(dosyanin_yeni_adi, firma_klasoru)
            move_file(dosyanin_yeni_adi, firma_klasoru)
